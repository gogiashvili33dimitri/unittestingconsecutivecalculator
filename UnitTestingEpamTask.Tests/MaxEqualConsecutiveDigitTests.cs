using Xunit;

namespace UnitTestingEpamTask.Tests
{
    public class MaxEqualConsecutiveDigitTests
    {
        [Theory]
        [InlineData("", 0)]
        [InlineData("    ", 0)]
        [InlineData(null, 0)]
        public void MaxEqualConsecutiveDigitEmptyOrNullStringReturnsZero(string input, int expected)
        {
            // Arrange

            // Act 
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("a", 0)]
        [InlineData("#", 0)]
        public void MaxEqualConsecutiveDigitStringWithOneCharReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("11123445", 3)]
        [InlineData("2222aA#33bCe", 4)]
        public void MaxEqualConsecutiveDigitMaxSequenceAtTheBegginingRetursCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData("12345555543221", 5)]
        [InlineData("#21333ZcxA33", 3)]
        public void MaxEqualConsecutiveDigitMaxSequenceInMiddleRetursCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("123456666", 4)]
        [InlineData("zc#1233C444", 3)]
        public void MaxEqualConsecutiveDigitMaxSequenceAtTheEndRetursCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveDigit_MaxSequenceAtMultiplePlaces_ReturnsCorrectCount()
        {
            // Arrange 
            string input = "111123Cx#s22223333";
            int expected = 4;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveDigitStringWithNoDigitsReturnsZero()
        {
            // Arrange
            string input = "aBcZ##$%!_";
            int expected = 0;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveDigitLongStringWithNoDigitsReturnsZero()
        {
            // Arrange
            string input = new string('a', 100_000);
            int expected = 0;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveDigitLongStringWithDigitsReturnsStringLength()
        {
            // Arrange
            int stringLength = 100_000;
            string input = new string('1', stringLength);
            int expected = stringLength;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveDigit(input);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}