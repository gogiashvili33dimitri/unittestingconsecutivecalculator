﻿using Xunit;

namespace UnitTestingEpamTask.Tests
{
    public class MaxUnequalConsecutiveCharsTests
    {
        [Theory]
        [InlineData("", 0)]
        [InlineData("    ", 0)]
        [InlineData(null, 0)]
        public void MaxUnequalConsecutiveCharsEmptyOrNullStringReturnsZero(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert 
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("a", 1)]
        [InlineData("A", 1)]
        [InlineData("1", 1)]
        [InlineData("#", 1)]
        public void MaxUnequalConsecutiveCharsOneCharReturnsOne(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("AaAaAa", 6)]
        [InlineData("aAbBcCdD", 8)]
        [InlineData("aaA#bbB1_$", 5)]
        public void MaxUnequalConsecutiveCharsCaseSensitiveReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("abcddeeefgg", 4)]
        [InlineData("12345555", 5)]
        [InlineData("12#abBccdDabc", 7)]
        public void MaxUnequalConsecutiveCharsMaxSequenceAtTheBegginingReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("aabCcdef", 7)]
        [InlineData("11123456", 6)]
        [InlineData("aaa%12", 4)]
        public void MaxUnequalConsecutiveCharsMaxSequenceAtTheEndReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("aabcdeee", 5)]
        [InlineData("112345666", 6)]
        [InlineData("aaaAa1_3456bbaA", 10)]
        public void MaxUnequalConsecutiveCharsMaxSequenceInTheMiddleReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxUnequalConsecutiveCharsMaxSequenceInMultiplePlacesReturnsCorrectCount()
        {
            // Arrange 
            string input = "abccccdeeeefggg";
            int expected = 3;

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void MaxUnequalConsecutiveCharsStringWithUniqueCharsReturnsLengthOfTheString()
        {
            // Arrange
            string input = "123abcdefg#$%^";
            int expected = input.Length;

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxUnequalConsecutiveCharsLongStringReturnsCorrectCount()
        {
            // Arrange
            string input = new string('a', 100_000);
            int expected = 1;

            // Act
            int actual = ConsecutiveCalculator.MaxUnequalConsecutiveChars(input);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}