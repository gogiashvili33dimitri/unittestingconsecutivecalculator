﻿using Xunit;

namespace UnitTestingEpamTask.Tests
{
    public class MaxEqualConsecutiveLettersTests
    {
        [Theory]
        [InlineData("", 0)]
        [InlineData("    ", 0)]
        [InlineData(null, 0)]
        public void MaxEqualConsecutiveLettersEmptyOrNullStringReturnsZero(string input, int expected)
        {
            // Arrange
            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("a", 1)]
        [InlineData("A", 1)]
        [InlineData("1", 0)]
        [InlineData("#", 0)]
        public void MaxEqualConsecutiveLettersOneCharReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("AaAaAa", 0)]
        [InlineData("aaaaA", 4)]
        public void MaxEqualConsecutiveLettersCaseSensitiveRetursCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("aaaabcccbb", 4)]
        [InlineData("aaaA1aa3bb#", 3)]
        public void MaxEqualConsecutiveLettersMaxSequenceAtTheBegginingReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("abcddddefg", 4)]
        [InlineData("a#b#cccCd%4", 3)]
        public void MaxEqualConsecutiveLettersMaxSequenceInTheMiddleReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("abccdefggg", 3)]
        [InlineData("A#b2c&dDdddd", 4)]
        public void MaxEqualConsecutiveLettersMaxSequenceAtTheEndReturnsCorrectCount(string input, int expected)
        {
            // Arrange

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveLettersMaxSequenceAtMultiplePlacesReturnsCorrectCount()
        {
            // Arrange 
            string input = "aaabcdddeeefghhh";
            int expected = 3;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveLettersStringWithNoLettersReturnsZero()
        {
            // Arrange
            string input = "123#$%^&_";
            int expected = 0;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveLettersLongStringWithNoLettersReturnsZero()
        {
            // Arrange
            string input = new string('1', 100_000);
            int expected = 0;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MaxEqualConsecutiveLettersLongStringWithLettersReturnsCorrectCount()
        {
            // Arrange
            int stringLenth = 100_000;
            string input = new string('a', stringLenth);
            int expected = stringLenth;

            // Act
            int actual = ConsecutiveCalculator.MaxEqualConsecutiveLetters(input);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}