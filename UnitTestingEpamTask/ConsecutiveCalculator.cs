﻿namespace UnitTestingEpamTask
{
    public static class ConsecutiveCalculator
    {
        public static int MaxUnequalConsecutiveChars(string input)
        {
            int maximum = 1;
            int current = 1;

            if (string.IsNullOrWhiteSpace(input))
            {
                return 0;
            }

            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] != input[i - 1])
                {
                    current++;
                    maximum = Math.Max(maximum, current);
                }
                else
                {
                    current = 1;
                }
            }

            return maximum;
        }

        public static int MaxEqualConsecutiveLetters(string input)
        {
            int maximum = 0;
            int current = 1;

            if (string.IsNullOrWhiteSpace(input))
            {
                return maximum;
            }

            if (input.Length == 1)
            {
                return (char.ToLower(input[0]) >= 'a' && char.ToLower(input[0]) <= 'z') ? 1 : 0;
            }

            bool hasLetters = false;

            for (int i = 1; i < input.Length; i++)
            {
                if (char.ToLower(input[i]) >= 'a' && char.ToLower(input[i]) <= 'z' && (input[i] == input[i - 1]))
                {
                    current++;
                    hasLetters = true;
                    maximum = Math.Max(maximum, current);
                }
                else
                {
                    current = 1;
                }
            }

            return hasLetters ? maximum : 0;
        }

        public static int MaxEqualConsecutiveDigit(string input)
        {
            int maximum = 0;
            int current = 1;

            if (string.IsNullOrWhiteSpace(input))
            {
                return maximum;
            }

            if (input.Length == 1)
            {
                return char.IsDigit(input[0]) ? 1 : 0;
            }

            bool hasDigits = false;

            for (int i = 1; i < input.Length; i++)
            {
                if ((char.IsDigit(input[i]) && input[i] == input[i - 1]))
                {
                    current++;
                    hasDigits = true;
                    maximum = Math.Max(maximum, current);
                }
                else
                {
                    current = 1;
                }
            }

            return hasDigits? maximum : 0;
        }
    }
}